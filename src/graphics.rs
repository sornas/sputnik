//! ## Some words about coordinates
//!
//! We try to be nice about drawing outside the bounds of the screen.
//! Any writes out of bounds will simply do nothing, and any reads
//! out of bounds will return `(0, 0, 0, 0)`. For this reason, most
//! pixel values are passed in as [i16] checked to be >0 before being
//! cast to [usize].
//!
//! This is not the case for the sprite indexing `(sx, sy, sw, sh)`
//! in [sprite_offset]. It instead returns an error if the sprite
//! index is out of the bounds of the entire sprite. It can still
//! be drawn outside the screen though.

use std::path::PathBuf;

use mlua::Integer;
use mlua::Result as LuaResult;
use mlua::{Lua, Table};

use crate::table_to_color;
use crate::EngineContext;
use crate::Rgba;

// XXX more fonts than sprite sheets?
// (what does this mean?)
pub struct FontData {
    pub path: PathBuf,
    pub fg_color: Rgba,
    pub bg_color: Rgba,
    pub char_width: u16,
    pub char_height: u16,
    pub scale: u16,
    pub line_spacing: f64,
}

pub fn rectangle(
    ctx: &mut EngineContext,
    _: &Lua,
    color: &Table,
    x: Integer,
    y: Integer,
    w: Integer,
    h: Integer,
    // TODO: rotate as optional arg? maybe separately
) -> LuaResult<()> {
    let Ok(x) = i16::try_from(x) else {
        return Ok(());
    };
    let Ok(y) = i16::try_from(y) else {
        return Ok(());
    };
    let Ok(w) = i16::try_from(w) else {
        return Ok(());
    };
    let Ok(h) = i16::try_from(h) else {
        return Ok(());
    };
    for py in y..(y + h) {
        if py < 0 || py >= crate::HEIGHT as i16 {
            continue;
        };
        for px in x..(x + w) {
            if px < 0 || px >= crate::WIDTH as i16 {
                continue;
            };
            set_pixel(ctx, color, px as Integer, py as Integer)?;
        }
    }
    Ok(())
}

pub fn sprite(ctx: &mut EngineContext, path: &str, x: Integer, y: Integer) -> LuaResult<()> {
    let Ok(x) = i16::try_from(x) else {
        return Ok(());
    };
    let Ok(y) = i16::try_from(y) else {
        return Ok(());
    };
    // XXX use sprite_offset or a common inner function for this
    let sprite = ctx.assets.open_image(PathBuf::from(path)).unwrap();
    for dy in 0..sprite.height {
        if y + (dy as i16) < 0 || y + (dy as i16) >= crate::HEIGHT as i16 {
            continue;
        }
        for dx in 0..sprite.width {
            if x + (dx as i16) < 0 || x + (dx as i16) >= crate::WIDTH as i16 {
                continue;
            }
            let sprite_offset = (dy * sprite.width + dx) * 4;
            let frame_offset = ((y as usize + dy) * crate::WIDTH as usize + (x as usize + dx)) * 4;
            let frame = ctx.pixels.frame_mut();
            frame[frame_offset] = sprite.data[sprite_offset];
            frame[frame_offset + 1] = sprite.data[sprite_offset + 1];
            frame[frame_offset + 2] = sprite.data[sprite_offset + 2];
            frame[frame_offset + 3] = sprite.data[sprite_offset + 3];
        }
    }
    Ok(())
}

/// Draw only part of a sprite. Useful for sprite sheets (including fonts).
pub fn sprite_offset(
    ctx: &mut EngineContext,
    path: PathBuf,
    sx: Integer,
    sy: Integer,
    sw: Integer,
    sh: Integer,
    x: Integer,
    y: Integer,
) -> LuaResult<()> {
    let Ok(sx) = u16::try_from(sx) else {
        return Ok(());
    };
    let Ok(sy) = u16::try_from(sy) else {
        return Ok(());
    };
    let Ok(sw) = u16::try_from(sw) else {
        return Ok(());
    };
    let Ok(sh) = u16::try_from(sh) else {
        return Ok(());
    };
    let Ok(x) = i16::try_from(x) else {
        return Ok(());
    };
    let Ok(y) = i16::try_from(y) else {
        return Ok(());
    };

    let sprite = ctx.assets.open_image(path).unwrap();
    assert!(((sx + sw) as usize) < sprite.width);
    assert!(((sy + sh) as usize) < sprite.height);
    let frame = ctx.pixels.frame_mut();
    for dy in 0..sh as usize {
        if y + (dy as i16) < 0 || y + (dy as i16) >= crate::HEIGHT as i16 {
            continue;
        }
        for dx in 0..sw as usize {
            if x + (dx as i16) < 0 || x + (dx as i16) >= crate::WIDTH as i16 {
                continue;
            }
            let sprite_offset = ((sy as usize + dy) * sprite.width + sx as usize + dx) * 4;
            let frame_offset = ((y as usize + dy) * crate::WIDTH as usize + (x as usize + dx)) * 4;
            frame[frame_offset] = sprite.data[sprite_offset];
            frame[frame_offset + 1] = sprite.data[sprite_offset + 1];
            frame[frame_offset + 2] = sprite.data[sprite_offset + 2];
            frame[frame_offset + 3] = sprite.data[sprite_offset + 3];
        }
    }
    Ok(())
}

pub fn clear(ctx: &mut EngineContext) -> LuaResult<()> {
    // TODO use clear color
    ctx.pixels.frame_mut().fill(0x00);
    Ok(())
}

// TODO how to handle out of bounds?
pub fn set_pixel(ctx: &mut EngineContext, color: &Table, x: Integer, y: Integer) -> LuaResult<()> {
    let Ok(x) = i16::try_from(x) else {
        return Ok(());
    };
    let Ok(y) = i16::try_from(y) else {
        return Ok(());
    };
    if x < 0 || x as u16 > crate::WIDTH {
        return Ok(());
    }
    if y < 0 || y as u16 > crate::HEIGHT {
        return Ok(());
    }
    let x = x as u16;
    let y = y as u16;
    // TODO ext trait for color.to_rgba()
    let (r, g, b, a) = table_to_color(color)?;
    let offset = (y as usize * crate::WIDTH as usize + x as usize) * 4;
    let frame = ctx.pixels.frame_mut();
    frame[offset] = r;
    frame[offset + 1] = g;
    frame[offset + 2] = b;
    frame[offset + 3] = a;
    Ok(())
}

// TODO how to handle out of bounds?
pub fn get_pixel(ctx: &EngineContext, x: u16, y: u16) -> LuaResult<Rgba> {
    let err_color = (0, 0, 0, 0);
    let Ok(x) = i16::try_from(x) else {
        return Ok(err_color);
    };
    let Ok(y) = i16::try_from(y) else {
        return Ok(err_color);
    };
    if x < 0 || x as u16 > crate::WIDTH {
        return Ok(err_color);
    }
    if y < 0 || y as u16 > crate::HEIGHT {
        return Ok(err_color);
    }
    let x = x as u16;
    let y = y as u16;
    let offset = (y as usize * crate::WIDTH as usize + x as usize) * 4;
    let frame = ctx.pixels.frame();
    Ok((
        frame[offset],
        frame[offset + 1],
        frame[offset + 2],
        frame[offset + 3],
    ))
}

pub fn set_text(ctx: &mut EngineContext, config: &Table) -> LuaResult<()> {
    // TODO warnings if wrong type
    if let Ok(fg) = config.get("fg") {
        ctx.current_font.fg_color = table_to_color(&fg)?;
    }
    if let Ok(bg) = config.get("bg") {
        ctx.current_font.bg_color = table_to_color(&bg)?;
    }
    if let Ok(scale) = config.get("scale") {
        ctx.current_font.scale = scale;
    }
    if let Ok(line_spacing) = config.get("line_spacing") {
        ctx.current_font.line_spacing = line_spacing;
    }
    Ok(())
}

// TODO: some parts quite duplicated with set_pixel. some other parts, not so much.
pub fn char(ctx: &mut EngineContext, c: char, x: u16, y: u16) -> LuaResult<()> {
    assert!(c.is_ascii());
    let sprite = ctx
        .assets
        // TODO uh oh, stinky!!
        .open_image(ctx.current_font.path.clone())
        .unwrap();
    // we assume the layout of the sprite sheet
    let sx = c as u16 / 16 * ctx.current_font.char_width;
    let sy = c as u16 % 16 * ctx.current_font.char_height;
    let sw = ctx.current_font.char_width;
    let sh = ctx.current_font.char_height;
    let frame = ctx.pixels.frame_mut();
    for dy in 0..sh as usize {
        for dx in 0..sw as usize {
            let sprite_offset = ((sy as usize + dy) * sprite.width + sx as usize + dx) * 4;

            // foreground or background?
            let sprite_color = (
                sprite.data[sprite_offset],
                sprite.data[sprite_offset + 1],
                sprite.data[sprite_offset + 2],
                sprite.data[sprite_offset + 3],
            );
            let (r, g, b, a) = if sprite_color == (255, 255, 255, 255) {
                ctx.current_font.fg_color
            } else if sprite_color == (0, 0, 0, 255) {
                ctx.current_font.bg_color
            } else {
                // XXX do something interesting here
                (0, 0, 0, 0)
            };

            let scale = ctx.current_font.scale as usize;
            // TODO can parallel
            for scalex in 0..scale {
                for scaley in 0..scale {
                    let frame_offset = ((y as usize + (dy * scale) + scaley)
                        * crate::WIDTH as usize
                        + (x as usize + (dx * scale) + scalex))
                        * 4;
                    frame[frame_offset] = r;
                    frame[frame_offset + 1] = g;
                    frame[frame_offset + 2] = b;
                    frame[frame_offset + 3] = a;
                }
            }
        }
    }
    Ok(())
}

pub fn text(ctx: &mut EngineContext, text: &str, mut x: u16, mut y: u16) -> LuaResult<()> {
    let start_x = x;
    for ch in text.chars() {
        if ch.is_ascii_graphic() {
            // print it
            char(ctx, ch, x, y)?;
            x += ctx.current_font.char_width * ctx.current_font.scale;
        } else if ch == '\n' {
            y += ((ctx.current_font.char_height * ctx.current_font.scale) as f64
                * ctx.current_font.line_spacing)
                .round() as u16;
            x = start_x;
        } else {
            // print '?' or something
        }
    }
    Ok(())
}
