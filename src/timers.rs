use std::sync::Mutex;
use std::time::Duration;

use mlua::Error as LuaError;
use mlua::Function;
use mlua::Integer;
use mlua::OwnedFunction;
use mlua::Result as LuaResult;
use mlua::Table;

use crate::EngineContext;

pub struct Timers {
    timers: Vec<Timer>,
}

#[derive(Debug)]
pub struct Timer {
    delta: Duration,
    last_call: std::time::Instant,
    function: Option<OwnedFunction>,
    remove: bool,
}

impl Timers {
    pub fn new() -> Self {
        Self { timers: Vec::new() }
    }
}

pub fn register(ctx: &mut EngineContext, timer: &Table) -> LuaResult<usize> {
    let function: Function = timer.get("func")?;
    let frequency: f64 = timer.get("freq")?;
    let delta = std::time::Duration::from_secs_f64(1.0 / frequency);
    let timer = Timer {
        delta,
        last_call: std::time::Instant::now(),
        function: Some(function.into_owned()),
        remove: false,
    };
    ctx.timers.timers.push(timer);
    Ok(ctx.timers.timers.len() - 1)
}

pub fn remove(ctx: &mut EngineContext, timer_id: Integer) -> LuaResult<()> {
    let Ok(id) = usize::try_from(timer_id) else {
        return Ok(());
    };
    if id >= ctx.timers.timers.len() {
        return Ok(());
    }
    ctx.timers.timers[id].remove = true;

    Ok(())
}

pub fn update(ctx: &Mutex<EngineContext>) -> LuaResult<()> {
    let now = std::time::Instant::now();
    let mut i = 0;
    // here be dragons
    loop {
        let mut ctx_ = ctx.lock().unwrap();
        // Poor man's for loop
        if i >= ctx_.timers.timers.len() {
            break;
        }
        let timer = &mut ctx_.timers.timers[i];
        let delta = now.duration_since(timer.last_call);
        if delta > timer.delta {
            // Take the function out of the EngineContext so we can call it without
            // needing to lock.
            let function = timer.function.take().ok_or(LuaError::CallbackDestructed)?; // TODO custom errors
            timer.last_call = now;
            // Drop the lock
            drop(ctx_);
            // Now we can call the function without deadlocking.
            function.call((delta.as_secs_f64(),))?;
            // And now we can put the function back.
            let mut ctx_ = ctx.lock().unwrap();
            ctx_.timers.timers[i].function = Some(function);
        }
        i += 1;
    }

    // Remove callbacks that have been marked for removal.
    ctx.lock().unwrap().timers.timers.retain(|t| !t.remove);

    Ok(())
}
