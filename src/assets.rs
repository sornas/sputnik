use png::ColorType;
use std::collections::HashMap;
use std::fs::File;
use std::path::PathBuf;

pub struct Assets {
    loaded_assets: HashMap<PathBuf, AssetData>,
}

pub enum AssetData {
    Image(ImageData),
}

impl AssetData {
    pub fn as_image_data(&self) -> Option<&ImageData> {
        match self {
            AssetData::Image(data) => Some(data),
        }
    }
}

pub struct ImageData {
    pub data: Vec<u8>,
    pub width: usize,
    pub height: usize,
}

impl Assets {
    pub fn new() -> Self {
        Self {
            loaded_assets: HashMap::new(),
        }
    }

    // pub fn open_asset(&mut self, path: PathBuf) -> Result<(), ()> {
    //     println!("opening {path:?}");
    //     // guess asset type from file name
    //     let extension = path.extension().ok_or(())?;
    //     if extension == "png" {
    //         self.open_image(path)
    //     } else {
    //         panic!(
    //             "unknown asset type from extension {}",
    //             extension.to_string_lossy()
    //         )
    //     }
    // }

    pub fn open_image(&mut self, path: PathBuf) -> Result<&ImageData, ()> {
        // stacked borrows moment
        if self.loaded_assets.get(&path).is_some() {
            return Ok(self
                .loaded_assets
                .get(&path)
                .unwrap() // safe, just checked
                .as_image_data()
                .unwrap()); // TODO error here?
        }
        // lifted from png crate documentation
        let decoder = png::Decoder::new(File::open(&path).map_err(|_| ())?);
        let mut reader = decoder.read_info().map_err(|_| ())?;
        let info = reader.info();
        assert!(matches!(info.color_type, ColorType::Rgba));
        let width = info.width as usize;
        let height = info.height as usize;
        let mut data = vec![0; reader.output_buffer_size()];
        // TODO "an APNG might contain multiple frames" according to png crate documentation.
        let info = reader.next_frame(&mut data).map_err(|_| ())?;
        data.truncate(info.buffer_size());
        self.loaded_assets.insert(
            path.clone(),
            AssetData::Image(ImageData {
                data,
                width,
                height,
            }),
        );
        Ok(self
            .loaded_assets
            .get(&path)
            .unwrap()
            .as_image_data()
            .unwrap())
    }
}
