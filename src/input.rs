use mlua::Error as LuaError;
use mlua::Result as LuaResult;
use winit::event::VirtualKeyCode;

use crate::EngineContext;

pub fn to_virtual_keycode(s: &str) -> Option<VirtualKeyCode> {
    use VirtualKeyCode::*;
    match s.to_lowercase().as_str() {
        "a" => Some(A),
        "b" => Some(B),
        "c" => Some(C),
        "d" => Some(D),
        "e" => Some(E),
        "f" => Some(F),
        "g" => Some(G),
        "h" => Some(H),
        "i" => Some(I),
        "j" => Some(J),
        "k" => Some(K),
        "l" => Some(L),
        "m" => Some(M),
        "n" => Some(N),
        "o" => Some(O),
        "p" => Some(P),
        "q" => Some(Q),
        "r" => Some(R),
        "s" => Some(S),
        "t" => Some(T),
        "u" => Some(U),
        "v" => Some(V),
        "w" => Some(W),
        "x" => Some(X),
        "y" => Some(Y),
        "z" => Some(Z),
        "1" => Some(Key1),
        "2" => Some(Key2),
        "3" => Some(Key3),
        "4" => Some(Key4),
        "5" => Some(Key5),
        "6" => Some(Key6),
        "7" => Some(Key7),
        "8" => Some(Key8),
        "9" => Some(Key9),
        "0" => Some(Key0),
        "esc" => Some(Escape),
        "enter" | "return" => Some(Return),
        "space" | "spc" | " " => Some(Space),
        "left" => Some(Left),
        "up" => Some(Up),
        "right" => Some(Right),
        "down" => Some(Down),
        _ => None,
    }
}

pub fn update(ctx: &mut EngineContext) -> LuaResult<()> {
    ctx.winit_input.step_with_window_events(&ctx.winit_events);
    ctx.winit_events.clear();
    Ok(())
}

/// True iff the specified key was pressed down _this frame_.
pub fn pressed(ctx: &EngineContext, key: &str) -> LuaResult<bool> {
    let vkc =
        to_virtual_keycode(key).ok_or_else(|| LuaError::external(format!("unknown key: {key}")))?;
    Ok(ctx.winit_input.key_pressed(vkc))
}

/// True iff the specified key was released _this frame_.
pub fn released(ctx: &EngineContext, key: &str) -> LuaResult<bool> {
    let vkc =
        to_virtual_keycode(key).ok_or_else(|| LuaError::external(format!("unknown key: {key}")))?;
    Ok(ctx.winit_input.key_released(vkc))
}

/// True iff the specified key is currently pressed down.
pub fn down(ctx: &EngineContext, key: &str) -> LuaResult<bool> {
    let vkc =
        to_virtual_keycode(key).ok_or_else(|| LuaError::external(format!("unknown key: {key}")))?;
    Ok(ctx.winit_input.key_held(vkc))
}
