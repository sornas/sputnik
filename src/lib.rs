use std::path::PathBuf;
use std::rc::Rc;
use std::sync::Mutex;

use error_iter::ErrorIter;
use mlua::Result as LuaResult;
use mlua::{Lua, Table};
use pixels::{Pixels, SurfaceTexture};
use winit::dpi::LogicalSize;
use winit::event::Event;
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::{Window, WindowBuilder};
use winit_input_helper::WinitInputHelper;

mod assets;
mod graphics;
mod input;
mod timers;

use crate::assets::Assets;
use crate::graphics::FontData;
use crate::timers::Timers;

const WIDTH: u16 = 320;
const HEIGHT: u16 = 240;

type Rgba = (u8, u8, u8, u8);

fn log_error_sources<E: std::error::Error + 'static>(err: E) {
    for source in err.sources().skip(1) {
        println!("  Caused by: {source}");
    }
}

fn log_error<E: std::error::Error + 'static>(err: E) {
    println!("{err}");
    log_error_sources(err);
}

fn log_call_error<E: std::error::Error + 'static>(method_name: &str, err: E) {
    println!("error when calling {method_name}: {err}");
    log_error_sources(err);
}

fn table_to_color(table: &Table) -> LuaResult<Rgba> {
    if table.len()? == 3 {
        // RGB array
        Ok((table.get(1)?, table.get(2)?, table.get(3)?, 255))
    } else if table.len()? == 4 {
        // RGBA array
        Ok((table.get(1)?, table.get(2)?, table.get(3)?, table.get(4)?))
    } else if table.contains_key("r")? && table.contains_key("g")? && table.contains_key("b")? {
        if table.contains_key("a")? {
            todo!()
        } else {
            todo!()
        }
    } else if table.contains_key("c")?
        && table.contains_key("m")?
        && table.contains_key("y")?
        && table.contains_key("k")?
    {
        todo!()
    } else if table.contains_key("h")? && table.contains_key("s")? && table.contains_key("v")? {
        todo!()
    } else if table.contains_key("h")? && table.contains_key("s")? && table.contains_key("l")? {
        todo!()
    } else {
        todo!()
    }
}

pub struct EngineContext {
    pub pixels: Pixels,
    pub window: Window,
    pub winit_input: WinitInputHelper,
    pub winit_events: Vec<winit::event::WindowEvent<'static>>,
    pub assets: Assets,
    pub current_font: FontData,
    pub timers: Timers,
}

/// Setup et cetera
impl EngineContext {
    // TODO what is the generic in EventLoop<()>?
    pub fn new(event_loop: &EventLoop<()>) -> Self {
        let window = {
            let size = LogicalSize::new(WIDTH as f64, HEIGHT as f64);
            WindowBuilder::new()
                .with_title("Hello Pixels")
                .with_inner_size(size)
                .with_min_inner_size(size)
                .build(event_loop)
                .unwrap()
        };
        let pixels = {
            let window_size = window.inner_size();
            let surface_texture =
                SurfaceTexture::new(window_size.width, window_size.height, &window);
            Pixels::new(WIDTH as u32, HEIGHT as u32, surface_texture).unwrap()
        };
        let assets = Assets::new();

        Self {
            pixels,
            window,
            winit_input: WinitInputHelper::new(),
            winit_events: Vec::new(),
            assets,
            current_font: FontData {
                path: PathBuf::from("res/fonts/nes/font-8.png"),
                char_height: 8,
                char_width: 8,
                fg_color: (255, 255, 255, 255),
                bg_color: (0, 0, 0, 255),
                scale: 1,
                line_spacing: 1.0,
            },
            timers: Timers::new(),
        }
    }
}

#[mlua::lua_module]
pub fn sputnik(lua: &Lua) -> LuaResult<Table> {
    let event_loop = EventLoop::new();
    let ctx = Rc::new(Mutex::new(EngineContext::new(&event_loop)));
    let event_loop = Rc::new(Mutex::new(Some(event_loop)));

    let sputnik = lua.create_table().unwrap();
    let graphics = lua.create_table().unwrap();
    let input = lua.create_table().unwrap();
    let timers = lua.create_table().unwrap();

    macro_rules! insert_function {
        ($ctx:expr, $lua:expr, [ $( $table:expr, $key:literal, $f:expr ),* $(,)? ]) => {
            $(
            let ctx_ = Rc::clone($ctx);
            $table
                .set(
                    $key,
                    $lua.create_function(move |lua, args| {
                        $f(&mut ctx_.lock().unwrap(), &lua, args)
                    })?,
                )?;
            )*
        };
    }

    let ctx_ = Rc::clone(&ctx);
    sputnik.set(
        "start",
        lua.create_function(move |_lua, ()| start(Rc::clone(&event_loop), Rc::clone(&ctx_)))?,
    )?;

    insert_function! {
        &ctx, lua, [
            graphics, "clear", |ctx: &mut EngineContext, _lua, ()| {
                graphics::clear(ctx)
            },
            graphics, "rectangle", |ctx: &mut EngineContext, lua, (color, x, y, w, h)| {
                graphics::rectangle(ctx, lua, &color, x, y, w, h)
            },
            graphics, "sprite", |ctx: &mut EngineContext, _lua, (path, x, y): (String, _, _)| {
                graphics::sprite(ctx, &path, x, y)
            },
            graphics, "sprite_offset", |ctx: &mut EngineContext, _lua, (path, sx, sy, sw, sh, x, y): (String, _, _, _, _, _, _)| {
                graphics::sprite_offset(ctx, PathBuf::from(path), sx, sy, sw, sh, x, y)
            },
            graphics, "get_pixel", |ctx: &mut EngineContext, _lua, (x, y)| {
                graphics::get_pixel(ctx, x, y)
            },
            graphics, "set_pixel", |ctx: &mut EngineContext, _lua, (color, x, y)| {
                graphics::set_pixel(ctx, &color, x, y)
            },
            graphics, "set_text", |ctx: &mut EngineContext, _lua, (config,) | {
                graphics::set_text(ctx, &config)
            },
            graphics, "text", |ctx: &mut EngineContext, _lua, (text, x, y): (String, _, _) | {
                graphics::text(ctx, &text, x, y)
            },

            input, "update", |ctx: &mut EngineContext, _lua, ()| {
                input::update(ctx)
            },
            input, "pressed", |ctx: &mut EngineContext, _lua, (key,): (String,)| {
                input::pressed(ctx, &key)
            },
            input, "released", |ctx: &mut EngineContext, _lua, (key,): (String,)| {
                input::released(ctx, &key)
            },
            input, "down", |ctx: &mut EngineContext, _lua, (key,): (String,)| {
                input::down(ctx, &key)
            },

            timers, "register", |ctx: &mut EngineContext, _lua, (timer,)| {
                timers::register(ctx, &timer)
            },
            timers, "remove", |ctx: &mut EngineContext, _lua, (timer_id,)| {
                timers::remove(ctx, timer_id)
            },
        ]
    }

    sputnik.set("graphics", graphics)?;
    sputnik.set("input", input)?;
    sputnik.set("timers", timers)?;
    Ok(sputnik)
}

fn start(event_loop: Rc<Mutex<Option<EventLoop<()>>>>, ctx: Rc<Mutex<EngineContext>>) -> LuaResult<()> {
    event_loop.lock().unwrap().take().unwrap().run(move |event, _, control_flow| {
        match event {
            Event::WindowEvent {
                window_id: _,
                event,
            } => {
                #[cfg(debug_assertions)]
                let debug_repr = format!("{event:?}");
                if let Some(event) = event.to_static() {
                    ctx.lock().unwrap().winit_events.push(event)
                } else {
                    #[cfg(debug_assertions)]
                    eprintln!("event could not be static: {debug_repr}");
                }
            }
            Event::RedrawRequested(_) => {
                if let Err(err) = ctx.lock().unwrap().pixels.render() {
                    log_call_error("pixels.render", err);
                    *control_flow = ControlFlow::Exit;
                    return;
                }
            }
            _ => (),
        }

        // Update timers
        if let Err(err) = timers::update(&ctx) {
            log_error(err);
            *control_flow = ControlFlow::Exit;
            return;
        }

        // Handle input events
        let mut ctx = ctx.lock().unwrap();
        // Close events
        if ctx.winit_input.close_requested() || ctx.winit_input.destroyed() {
            *control_flow = ControlFlow::Exit;
            return;
        }

        // Resize the window
        if let Some(size) = ctx.winit_input.window_resized() {
            if let Err(err) = ctx.pixels.resize_surface(size.width, size.height) {
                log_call_error("pixels.resize_surface", err);
                *control_flow = ControlFlow::Exit;
                return;
            }
        }

        ctx.window.request_redraw();
    });
}
