Sputnik is a pixel-based game engine primarily intended for game jams.

You can find some basic example games in the `examples/`-directory. Information
and controls are written as a comment at the top of each game. Nothing is done,
really, but I'm pretty sure where I want to take this engine. Basically, I have
three levels of planning:

## Building from source

For initial development, we only support building manually from source. Down
the line we will have compiled modules that can be distributed.

Pre-requisites:

- Rust and Cargo (installed via Rustup either from your distribution or
  http://rustup.rs is recommended).
- Lua

1. `$ git clone https://gitlab.com/sputnik-engine/sputnik`
2. `$ cd sputnik`
3. `$ cargo build`
4. `$ cp target/debug/libsputnik.so sputnik.so`
5. `$ lua examples/pong.lua`

You can also use some other language that can understand Lua modules if you
want to.

## Long-term

Some ideas / long-term plans.

I would like to have integrated level, sprite and audio editing, like PICO-8
has. Unlike PICO-8, I want it to be optional, meaning you can just as well edit
outside the editors. (Why force people to use my sub-par sprite editor when
Aseprite exists?) For sprites this is easy (just write PNGs), and levels
should be fine (just write PNGs). Audio is more difficult, but goes into the
next paragraph.

My experience with game jams is that we have a music person (could be a
programmer who wants to take a break) who does all the music and uploads WAVs to
the repository. The music is created in some other tool and kept separately. In
one group the music person jumped in, basically just asked what the vibe was
going to be, and then sent us some (very good!) background music we could use
later that day. But I want a system that is more collaborative. I want to
generate the music in-engine from a textual representation. This is basically a
whole separate project. (Maybe it _should_ be separate.)

Making the music generator separate would mean having some kind of plugin/module
system, where module could be written and compiled separately. I'm not sure
exactly how it would work right now. The easiest would be to compile a
Lua-module that is `require`d by the game, and registered to the engine in some
way. Either manually (`sputnik.modules.register(...)`) or automatically (the
module should be able to reference the global `sputnik` too, right?).

For integrated editors, it would be nice if we could jump between "games" (or
"runnables" or whatever). If all state stays in Lua, it might be as easy as
running another `sputnik.setup`. Probably we have to be a little bit more
clever.

Asset reloading is a nice productivity boost that just Makes Sense when the
assets are supposed to be pretty small. Hot code reloading would be cool too
(arguably a better productivity boost) but
I'm not sure how Lua handles that.
