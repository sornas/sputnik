sputnik = require "sputnik"

draw = ->
    sputnik.graphics.clear!

update = (_dt) ->
    sputnik.input.update!
    print "update"
    draw!

sputnik.timers.register {
    func: update
    freq: 60
}

sputnik.start!
