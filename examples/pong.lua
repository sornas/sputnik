--[[
  Well, this one needs no introduction.

  TODO:
  - Ball hit direction

  Controls:
  - W: player 1 up
  - S: player 1 down
  - E: player 1 serve

  - I: player 2 up
  - K: player 2 down
  - U: player 2 serve
]]
--

local sputnik = require "sputnik"

local screen_width = 320
local screen_height = 240

local player_width = 10
local player_height = 40

local world = {
  -- TODO player constructor
  players = { {
    x = 10,
    y = 10,
    w = player_width,
    h = player_height,
  }, {
    x = screen_width - player_width - 10,
    y = 10,
    w = player_width,
    h = player_height,
  } },
  ball = {
    x = screen_width / 2,
    y = screen_height / 2,
    w = 10,
    h = 10,

    dx = 1,
    dy = 0,

    -- waiting for player N to start
    waiting_on = 1,
  },
  score = { 0, 0 },
}

local function collision(box1, box2)
  return box1.x < box2.x + box2.w and box2.x < box1.x + box1.w and box1.y < box2.y + box2.h and box2.y < box1.y + box1.h
end

function world:update(dt)
  self.players:update(dt)

  -- resolving ball-paddle collisions:
  --[[
          +--+  player.y
          |  |
    < +--+|  |  ball.y
    < |  ||  |
    < +--+|  |  ball.y + ball.h
          |  |
          |  |
          |  |
          |  |
          +--+ player.y + player.h

    ball.y = player.y - ball.h => 0.5
    ball.y = player.y + player.h => -0.5

    ball.y in range from -ball.h to +player.h
    (val + low) / high - 1

    in practice: a bit too sensitive around 0 (would like to dampen) but hey,
    it's an example.
  ]]

  -- check ball collision
  if self.ball.dx > 0 then -- moving right
    -- check with left paddle
    if collision(self.ball, self.players[2]) then
      local diff = self.ball.y - self.players[2].y
      local normalized = (diff + self.ball.h / 2) / self.players[2].h
      local ranged = normalized - 0.5
      self.ball.dy = ranged / math.sqrt(math.abs(ranged) + 1)
      self.ball.dx = -1 / math.sqrt(math.abs(ranged) + 1)
    end
    -- with right wall
    if self.ball.x + self.ball.w > screen_width then
      self.score[1] = self.score[1] + 1
      self.ball.x = screen_width / 2
      self.ball.y = screen_height / 2
      self.ball.waiting_on = 2
      self.ball.dx = 0
      self.ball.dy = 0
    end
  else -- moving left
    -- check with right paddle
    if collision(self.ball, self.players[1]) then
      local diff = self.ball.y - self.players[1].y
      local normalized = (diff + self.ball.h / 2) / self.players[1].h
      local ranged = normalized - 0.5
      self.ball.dy = ranged / math.sqrt(math.abs(ranged) + 1)
      self.ball.dx = 1 / math.sqrt(math.abs(ranged) + 1)
    end
    -- with left wall
    if self.ball.x < 0 then
      self.score[2] = self.score[2] + 1
      self.ball.x = screen_width / 2
      self.ball.y = screen_height / 2
      self.ball.waiting_on = 1
      self.ball.dx = 0
      self.ball.dy = 0
    end
  end
  -- with top wall
  if self.ball.y <= 0 and self.ball.dy < 0 then
    self.ball.dy = -self.ball.dy
  end
  -- with bottom wall
  if self.ball.y + self.ball.h >= screen_height and self.ball.dy > 0 then
    self.ball.dy = -self.ball.dy
  end

  self.ball:update(dt)
end

function world:draw()
  self.players:draw()
  self.ball:draw()
  self.score:draw()
end

function world.players:update(dt)
  -- player 1
  local p1 = self[1]
  local p2 = self[2]
  if sputnik.input.down("w") then
    p1.y = p1.y - 100 * dt
    if p1.y < 0 then
      p1.y = 0
    end
  end
  if sputnik.input.down("s") then
    p1.y = p1.y + 100 * dt
    if p1.y + p1.h > screen_height then
      p1.y = screen_height - p1.h
    end
  end
  if sputnik.input.down("i") then
    p2.y = p2.y - 100 * dt
    if p2.y < 0 then
      p2.y = 0
    end
  end
  if sputnik.input.down("k") then
    p2.y = p2.y + 100 * dt
    if p2.y + p2.h > screen_height then
      p2.y = screen_height - p2.h
    end
  end
end

function world.ball:update(dt)
  if self.waiting_on == 1 and sputnik.input.down("e") then
    self.dx = -1
  end
  if self.waiting_on == 2 and sputnik.input.down("u") then
    self.dx = 1
  end

  self.x = self.x + self.dx * 100 * dt
  self.y = self.y + self.dy * 100 * dt
end

function world.players:draw()
  sputnik.graphics.rectangle({ 255, 255, 255 }, self[1].x, self[1].y, self[1].w, self[1].h)
  sputnik.graphics.rectangle({ 255, 255, 255 }, self[2].x, self[2].y, self[2].w, self[2].h)
end

function world.ball:draw()
  sputnik.graphics.rectangle({ 255, 255, 255 }, self.x, self.y, self.w, self.h)
end

function world.score:draw()
  sputnik.graphics.text(string.format("%d", self[1]), 50, 20)
  sputnik.graphics.text(string.format("%d", self[2]), screen_width - 50 - 8 * 4, 20)
end

local function setup()
  sputnik.graphics.set_text {
    fg = { 255, 255, 255, 255 },
    bg = { 0, 0, 0, 255 },
    scale = 4,
    line_spacing = 1.5,
  }
end

local function draw()
  sputnik.graphics.clear()
  world:draw()
end

local function update(dt)
  sputnik.input.update()
  world:update(dt)

  draw()
end

--

setup()
sputnik.timers.register {
  func = update,
  freq = 60,
}
sputnik.start()
